var express = require ('express');
var passport = require('passport');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var bodyParser = require('body-parser');
var mongoose = require ("mongoose");
var config = require('./config/database');
var Login = require('./models/login');
var Bookings = require('./models/bookings');
var jwt = require('jwt-simple');
var session = require('express-session');
var redirect = require('express-redirect');

//connect to database
mongoose.connect(config.database);


//bundle our routes
var apiRoutes = express.Router();

app.use(bodyParser.urlencoded({
	extended:true
}));
app.use(bodyParser.json());

//log to console
app.use(morgan('dev'));

//use the passport package 
app.use(passport.initialize());

app.use(session({secret:"alumniwebapplication",resave:false,saveUninitialized:true}))


//API TO DISPLAY LOGIN PAGE AS HOME PAGE
app.get('/', function(req, res){
	res.sendFile(__dirname + '/public/login.html');
});


//API TO POST REQUEST LOGIN 
apiRoutes.post('/login', function(req, res) {
	var username = req.body.username;
	var password = req.body.password;

	
	Login.findOne({username: username, password: password},function(err,login){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(!login){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("Error");
			res.json({status:0, message:'INVALID LOGIN CREDENTIALS'});
		}

		else{
			
			req.session.login=login;
			if(username==='alumni'){
				//res.sendFile(__dirname+'/public/alumni_dashbord.html');
				console.log("ok");
				res.json({status:1});
				//res.redirect('c:/project/Alumni-web/public/alumni_dashbord.html');
			}
			else{
				console.log("ok");
				res.json({status:2});
				//res.redirect('/public/student_dashbord.html');
				//res.sendFile(__dirname+'/public/student_dashbord.html');
			}	
		}
	});

});


//API TO DISPLAY ALUMNI DASHBOARD
app.get('/alumni_dashboard', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	res.sendFile(__dirname + '/public/alumni_dashboard.html');
});


//API TO CHECK FOR EXISTING BOOKING 
app.get('/booking_check', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	
	Bookings.findOne({Status:'pending'},function(err,exist){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(exist){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("Pending requests exist");
			res.json({status:0, message:'Cannot Continue to book'});
		}

		else{
			
			console.log("ok");
			res.json({status:1,message:'no pending requests'});
			
		}
	});
});


//API TO CONFIRM OR REJECT STUDENT'S BOOKING 
app.get('/confirm_requests', function(req, res){
	var arraycount=0;
	if(!req.session.login){
		return res.status(401).send();
	}
	
	Bookings.find({Status:'pending'},function(err,result){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(!result){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("NO Pending requests exist");
			res.json({status:0, message:'NO Current Booking Requests'});
		}

		else{
			Bookings.count({Status:'pending'},function(err,pcount){
				if(err){
					console.log("Error in count:"+err);
					res.json({message:'Error in count'});
				}
				arraycount=pcount;
				console.log(pcount);
				console.log(arraycount);
				res.json({status:1,message:'Pending Requests exist',count:pcount, data:{result}});
			});
			console.log("ok");
		}
	});
});

//API TO ACCEPT STUDENT'S BOOKING 
app.post('/accept', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	var studentname = req.body.studentname;
	var date = req.body.date;
	var time_slot = req.body.time_slot;
	var status = req.body.status;
	
	Bookings.find({Status:'pending',studentname:studentname,Booking_date:date,Booking_timeslot:time_slot},function(err,result){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(!result){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("NO  requests found");
			res.json({status:0, message:'NO Such booking found'});
		}

		else{
			var myquery1= {Status:'pending',studentname:studentname,Booking_date:date,Booking_timeslot:time_slot};
			var newvalues = { $set: { Status:status } };
			Bookings.updateOne(myquery1, newvalues, function(err, result1) {
				if (err) throw err;
				console.log(result1);
				if(result1){
					console.log("Booking Accepted");
					res.json({code:200, message:' booking Successfully Accepted'});
				}


			});
			console.log("ok");
		}
	});
});


//API TO REJECTT STUDENT'S BOOKING 
app.post('/reject', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	var studentname = req.body.studentname;
	var date = req.body.date;
	var time_slot = req.body.time_slot;
	var status = req.body.status;
	
	Bookings.find({Status:'pending',studentname:studentname,Booking_date:date,Booking_timeslot:time_slot},function(err,result){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(!result){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("NO  requests found");
			res.json({status:0, message:'NO Such booking found'});
		}

		else{
			var myquery1={Status:'pending',studentname:studentname,Booking_date:date,Booking_timeslot:time_slot};
			var newvalues = { $set: { Status:status } };
			Bookings.updateOne(myquery1, newvalues, function(err, result1) {
				if (err) throw err;
				if(result1)
					console.log("Booking REJECTED");
				res.json({code:200, message:' booking Successfully REJECTED'});
			});
			console.log("ok");
		}
	});
});

//API TO CHECK AND MAKE STUDENT'S BOOKING
app.post('/book', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	
	Bookings.count({studentname:req.session.login.username,  Status:'Accepted'},function(err,count){
		if(err){
			console.log(err);
			return res.status(500).send();
		}
		if(count>=2){

			//return res.status(400).send('INVALID LOGIN CREDENTIALS');
			console.log("Reached maximum booking limit");
			res.json({status:0, message:'Cannot Continue to book'});
		}

		else{
			var newBookings = new Bookings({
				studentname:req.session.login.username,
				Booking_date:req.body.date,	
				Booking_timeslot:req.body.time_slot,
				Status:req.body.status
			});

			Bookings.findOne({studentname:req.session.login.username,Booking_date:req.body.date,Booking_timeslot:req.body.time_slot,Status:'Accepted'},function(err,exist){
				if(err){
					console.log(err);
					return res.status(500).send();
				}

				if(exist){
					console.log("Booking of same date exist");
					return res.json({status:2, success: false, msg: 'Booking of same date exist'});
				}
				else{
					console.log(newBookings.Status);
					newBookings.save(function(err,booking) {
						if (err) {
							console.log(err);
							return res.json({code:401, success: false, msg: 'Error occured.'});
						}  	    
						res.json({ code: 200,status:1, success:true, msg: 'Successfully created new booking' });	
					}); 
				}
			});

			
			 	
			console.log("ok");
		}
	});
});


app.get('/student_dashboard', function(req, res){
	if(!req.session.login){
		return res.status(401).send();
	}
	res.sendFile(__dirname + '/public/student_dashboard.html');
});


// route for user logout
app.get('/logout', (req, res) => {
	req.session.destroy();
	res.send("logout success!");
});




// connect the user routes under /user/*
app.use('', apiRoutes);


//start the server
app.listen(port,()=>{
	console.log("Server listening on port" + port);
});