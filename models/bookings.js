var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var BookingsSchema = new Schema({
	studentname: {
		type: String,
		unique:false,
		required: true
	},
	Booking_date:{
		type:String,
		unique:false,
		required:false
	},
	Booking_timeslot :{
		type:String,
		unique:false,
		required:false
	},
	Status:{
		type:String,
		unique:false
	}
});

module.exports = mongoose.model('Bookings',BookingsSchema);