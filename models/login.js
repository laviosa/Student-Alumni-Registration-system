var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var LoginSchema = new Schema({
	username: {
		type: String,
		unique:true,
		required: true
	},
	password :{
		type:String,
		required:true
	}
	},
	{ collection : 'login' });

module.exports = mongoose.model('Login',LoginSchema);